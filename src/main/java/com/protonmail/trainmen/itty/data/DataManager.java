package com.protonmail.trainmen.itty.data;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;

public final class DataManager
{
	private static final Gson GSON = new GsonBuilder()
			.setPrettyPrinting()
			.serializeNulls()
			.create();

	private DataManager()
	{
	}

	public static <T> T load(File file, Class<T> tClass)
	{
		if (!file.exists())
		{
			return null;
		}

		try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file)))
		{
			T instance = GSON.fromJson(bufferedReader, tClass);
			if (instance != null)
			{
				return instance;
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public static <T> void save(T instance, File file, Class<T> tClass)
	{
		try
		{
			if (!file.exists())
			{
				file.getParentFile().mkdirs();
				file.createNewFile();
			}

			try (FileWriter fileWriter = new FileWriter(file))
			{
				GSON.toJson(instance, fileWriter);
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
