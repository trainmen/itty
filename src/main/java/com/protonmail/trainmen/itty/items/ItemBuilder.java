package com.protonmail.trainmen.itty.items;

import com.google.common.collect.Lists;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

public class ItemBuilder
{
	private Material material;
	private int amount;
	private String displayName;
	private List<String> lore;

	public ItemBuilder()
	{
	}

	public ItemBuilder(Material material)
	{
		this.material = material;
	}

	public ItemBuilder(Material material, short amount)
	{
		this.material = material;
		this.amount = amount;
	}

	public ItemBuilder(Material material, short amount, String displayName)
	{
		this.material = material;
		this.amount = amount;
		this.displayName = displayName;
	}

	public ItemBuilder withMaterial(Material material)
	{
		this.material = material;
		return this;
	}

	public ItemBuilder withAmount(int amount)
	{
		this.amount = amount;
		return this;
	}

	public ItemBuilder withDisplayName(String displayName)
	{
		this.displayName = displayName;
		return this;
	}

	public ItemBuilder withLore(List<String> lore)
	{
		this.lore = lore;
		return this;
	}

	public ItemBuilder withLoreLine(String loreLine)
	{
		if (lore == null)
		{
			lore = Lists.newArrayList();
		}
		lore.add(loreLine);
		return this;
	}

	public ItemStack build()
	{
		ItemStack item = new ItemStack(Material.AIR);
		item.setType(material);
		item.setAmount(amount);
		ItemMeta meta = item.getItemMeta();

		if (displayName != null)
		{
			meta.setDisplayName(displayName);
		}

		if (lore != null)
		{
			meta.setLore(lore);
		}

		item.setItemMeta(meta);
		return item;
	}
}
