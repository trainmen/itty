package com.protonmail.trainmen.itty.items;

import org.apache.commons.lang.ArrayUtils;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

public final class ItemTags
{
	private static final NamespacedKey KEY_ITTY_ITEM_TAGS = NamespacedKey.minecraft("itty_item_tags");

	public static void addTag(ItemStack itemStack, String tag)
	{
		ItemMeta itemMeta = itemStack.getItemMeta();
		PersistentDataContainer dataContainer = itemMeta.getPersistentDataContainer();

		int[] tags;

		if (dataContainer.has(KEY_ITTY_ITEM_TAGS, PersistentDataType.INTEGER_ARRAY))
		{
			tags = dataContainer.get(KEY_ITTY_ITEM_TAGS, PersistentDataType.INTEGER_ARRAY);
			if (!ArrayUtils.contains(tags, tag.hashCode()))
			{
				tags = ArrayUtils.add(tags, tag.hashCode());
			}
		}
		else
		{
			tags = new int[]{tag.hashCode()};
		}

		dataContainer.set(KEY_ITTY_ITEM_TAGS, PersistentDataType.INTEGER_ARRAY, tags);
		itemStack.setItemMeta(itemMeta);
	}

	public static void removeTag(ItemStack itemStack, String tag)
	{
		ItemMeta itemMeta = itemStack.getItemMeta();
		PersistentDataContainer dataContainer = itemMeta.getPersistentDataContainer();

		if (!dataContainer.has(KEY_ITTY_ITEM_TAGS, PersistentDataType.INTEGER_ARRAY))
		{
			return;
		}

		int[] tags = dataContainer.get(KEY_ITTY_ITEM_TAGS, PersistentDataType.INTEGER_ARRAY);
		if (ArrayUtils.contains(tags, tag.hashCode()))
		{
			if (tags.length == 1)
			{
				tags = new int[0];
			}
			else
			{
				tags = ArrayUtils.remove(tags, tag.hashCode());
			}
		}

		if (tags.length == 0)
		{
			dataContainer.remove(KEY_ITTY_ITEM_TAGS);
		}
		else
		{
			dataContainer.set(KEY_ITTY_ITEM_TAGS, PersistentDataType.INTEGER_ARRAY, tags);
		}
		itemStack.setItemMeta(itemMeta);
	}

	public static boolean hasTag(ItemStack itemStack, String tag)
	{
		ItemMeta itemMeta = itemStack.getItemMeta();
		PersistentDataContainer dataContainer = itemMeta.getPersistentDataContainer();

		if (!dataContainer.has(KEY_ITTY_ITEM_TAGS, PersistentDataType.INTEGER_ARRAY))
		{
			return false;
		}

		int[] tags = dataContainer.get(KEY_ITTY_ITEM_TAGS, PersistentDataType.INTEGER_ARRAY);
		return ArrayUtils.contains(tags, tag.hashCode());
	}

	private ItemTags()
	{
	}
}
