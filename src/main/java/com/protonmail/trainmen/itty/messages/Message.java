package com.protonmail.trainmen.itty.messages;

import org.bukkit.ChatColor;

public final class Message
{
	public static String newColored(ChatColor color, String format, Object... args)
	{
		if (args.length == 0)
		{
			return color + format;
		}
		else
		{
			return color + String.format(format, args);
		}
	}

	public static String newInfo(String format, Object... args)
	{
		return newColored(ChatColor.WHITE, format, args);
	}

	public static String newSuccess(String format, Object... args)
	{
		return newColored(ChatColor.GREEN, format, args);
	}

	public static String newError(String format, Object... args)
	{
		return newColored(ChatColor.RED, format, args);
	}

	public static String newNoPermission(String label)
	{
		return newError("You do not have permission to use /%s.", label.toLowerCase());
	}

	public static String newNotEnoughArgs(String label, int minArgs)
	{
		return newError("You must provide at least %d argument to /%s.", minArgs, label.toLowerCase());
	}

	public static String newPlayerOnly(String label)
	{
		return newError("You must be a player to use /%s.", label);
	}

	public static String newNoSuchPlayer(String playerName)
	{
		return newError("Could not find player '%s'.", playerName);
	}

	private Message()
	{
	}
}
